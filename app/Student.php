<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $guarded = ['id'];


    public function answers() {
        return $this->hasMany(Answer::class);
    }
}
