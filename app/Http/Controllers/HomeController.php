<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\QuestionStoreRequest;
use App\Category;
use App\Option;
use App\Question;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $category = Category::all()->pluck('name', 'id');

        return view('admin.home', compact('category'));
    }

    public function saveQuestion (QuestionStoreRequest $request) {
        $question = new Question();
        $question->title = $request->question_name;
        $question->category_id = $request->question_cat_id;
        $question->answer_ids = $request->question_answer;
        $question->save();

        foreach($request->question_options as $value){
            $option = new Option();
            $option->question_id = $question->id;
            $option->title = $value;
            $option->save();
        }

        return back()->with('message', 'Data Added successfully.');
    }
}
