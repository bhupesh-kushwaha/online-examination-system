<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Question;
use App\Student;
use App\Http\Requests\studentAnswerStoreRequest;
use App\Answer;

class studentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()   
    {

    }

    public function index() {
        $category = Category::all()->pluck('name', 'id');

        return view('welcome', compact('category'));
    }

    public function showMCQ(Category $id)
    {
        $category = $id;

        $categoryName = $category->name;
        
        $questionLists = $category->questions;

        return view('student.student', compact(
            'categoryName',
            'questionLists'
        ));
    }

    public function saveMCQ(studentAnswerStoreRequest $request)
    {
        $getQuestions = [];

        foreach ($request->except('_token') as $key => $part) {
            if( stristr( $key,'i-q-') !== false ) {
                $getQuestions[] = [
                    'id' => $key,
                    'value' => $part
                ];
            }
        }

        $student = new Student();
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->email = $request->email;
        $student->save();

        $request->session()->put('studentData', 
            [
                'id' => $student->id,
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'email' => $request->input('email')
            ]
        );

        foreach($getQuestions as $aq){
            $question = new Answer();
            $question->student_id = $student->id;
            $question->question_id = substr( $aq['id'], strrpos( $aq['id'], "-" ) + 1 );
            $question->answer_id = $aq['value'];
            $question->save();
        }

        return redirect()->route('mcqView')->with('message', 'Data Added successfully.');
    }

    public function viewMCQ()
    {
        $studentData = \Session::get('studentData');

        $studentAnswers = Answer::where('student_id', $studentData['id'])->get();

        return view('student.view', compact(
            'studentAnswers',
            'studentData'
        ));
    }
}
