<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $guarded = ['id'];

    public function students() {
        return $this->belongsTo(Student::class);
    }

    public function questions() {
        return $this->belongsTo(Question::class, 'question_id')->with('categories');
    }
}
