<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $guarded = ['id'];

    public function categories() {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function options() {
        return $this->hasMany(Option::class);
    }
}
