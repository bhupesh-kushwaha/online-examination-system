@if( Session::has('errors') )
    <div class="alert alert-danger" role="alert" align="left">
        <div class="errlist">
        @foreach($errors->all() as $error) 
            {{$error}}</br>
        @endforeach
        </div>
    </div>
@endif

@if( Session::has('message') )
    <div class="alert alert-success" role="alert">
        {{ Session::get('message') }}
    </div>
@endif

@if( Session::has('error') )
    <div class="alert alert-danger" role="alert">
        {{ Session::get('error') }}
    </div>
@endif