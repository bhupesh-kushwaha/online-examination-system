@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    @include('alert')

                    <form action="{{route('saveQuestion')}}" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        
                        <div class="col-md-12">
                                                                                                                                                                                                                        
                            <div class="form-group">
                                <label for="">Category</label>
                                <select
                                    class="form-control"
                                    name="question_cat_id"
                                >
                                    <option>Select Category</option>

                                    @foreach($category as $key=>$val)
                                         @if (old('question_cat_id') == $key)
                                             <option value="{{ $key }}" selected>{{ $val }}</option>
                                         @else
                                             <option value="{{ $key }}">{{ $val }}</option>
                                         @endif
                                    @endforeach

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Question</label>
                                <input 
                                    name="question_name" 
                                    type="text" 
                                    value="{{old('question_name')}}" 
                                    class="form-control"
                                />
                            </div>                          

                            <div class="form-group">
                                <label for="">Option 1</label>
                                <input 
                                    name="question_options[]" 
                                    type="text" 
                                    class="form-control"
                                />
                            </div>                          

                            <div class="form-group">
                                <label for="">Option 2</label>
                                <input 
                                    name="question_options[]" 
                                    type="text" 
                                    class="form-control"
                                />
                            </div>                          

                            <div class="form-group">
                                <label for="">Option 3</label>
                                <input 
                                    name="question_options[]" 
                                    type="text" 
                                    class="form-control"
                                />
                            </div>                          

                            <div class="form-group">
                                <label for="">Option 4</label>
                                <input 
                                    name="question_options[]" 
                                    type="text" 
                                    class="form-control"
                                />
                            </div>

                            <div class="form-group">
                                <label for="">Answer</label>
                                <input 
                                    name="question_answer" 
                                    type="text" 
                                    value="{{old('question_answer')}}" 
                                    class="form-control"
                                />
                            </div>
                        </div>
                       
                        <div class="col-md-6">
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
