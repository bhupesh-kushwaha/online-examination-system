@extends('student.parent')

@section('main')

                    
@include('alert')

<div align="right">
	<a class="btn btn-primary" href="{{ url('/') }}" class="btn btn-default">Back</a>
</div>
<hr/>
<form id="studentForm" method="post" action="{{ route('mcqSave') }}" enctype="multipart/form-data">

	@csrf

	<div class="panel-group">
   	 	<div class="panel panel-default">
   	 	  	<div class="panel-heading">
				  Student Details
			</div>
   	 	  	
			<div class="panel-body">

				<div class="form-group">
					<div class="col-sm-4">
						<label for="firstname" class="sr-only"></label>
						<input 
							id="firstname" 
							class="form-control input-group-lg" 
							type="text" 
							name="first_name" 
							title="Enter first name" 
							placeholder="First name"
							required
							value="{{old('first_name')}}" 
						/>
					</div>       

					<div class="col-sm-4">
						<label for="lastname" class="sr-only"></label>
						<input 
							id="lastname" 
							class="form-control input-group-lg" 
							type="text" 
							name="last_name" 
							title="Enter last name" 
							placeholder="Last name"
							required
							value="{{old('last_name')}}" 
						/>
					</div>

					<div class="col-sm-4">
						<label for="email" class="sr-only"></label>
						<input 
							id="email" 
							class="form-control input-group-lg" 
							type="email" 
							name="email" 
							title="Enter email" 
							placeholder="Email"
							required
							value="{{old('email')}}" 
						/>
					</div>
				</div>
			</div>
   	 	</div>

 	  	<div class="panel panel-default">
 	  	  	<div class="panel-heading">
				MCQ Test for {{ $categoryName }}
			</div>

 	  	 	<div class="panel-body mcq-lists">
				@php $i = 1; @endphp

				@forelse ($questionLists as $question)
    
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{ $i . "." . $question->title}}</h4>

						@foreach ($question->options as $options)
						<div class="form-check">
							<label class="form-check-label">
								<input type="checkbox" class="form-check-input options {{ 'q-' . $question->id  }}" name="{{ 'q-' . $question->id  }}[]" value="{{ $options->title }}"> {{ $options->title }}
							</label>
						</div>
						@endforeach

						<input 
							type="hidden"
							class="inputOption" 
							name="{{ 'i-q-' . $question->id  }}" 
							value="{{old('i-q-'.$question->id)}}"
						/>
						
					</div>
				</div>

				@php $i++; @endphp

				@empty
					Not found.
				@endforelse
				

				<button class="btn btn-danger" type="button" name="prev">previous</button>
				<button class="btn btn-primary" type="button" name="next">next</button>
				
			</div>
 	  	</div>

 	</div>

</form>

@endsection

@section('page-script')
<script type="text/javascript">
	function ajaxRequest() {

	}

	$(document).ready(function() {
		var divs = $('.mcq-lists > .card');
		var now = 0; 

		divs.hide().first().show(); 
		
		$("button[name=next]").click(function() {
			
			if( now ===  divs.length - 1) {
				$("#studentForm").submit(); 
				return false;
			}
			
			divs.eq(now).hide();

			now =  now + 1 ;
			divs.eq(now).show(); 
		});

		$("button[name=prev]").click(function() {
			divs.eq(now).hide();
			now = (now > 0) ? now - 1 : divs.length - 1;
			divs.eq(now).show(); 
		});

		$("body").on('change', '.options',  function() {
			var optionValues = "";

			$(this).closest('.card-body').find('input[type=checkbox]:checked').each ( function() {
				optionValues = optionValues + "," + $(this).val();
			});

			optionValues = (optionValues[0] == ',') ? optionValues.substr(1) : optionValues;

			$(this).closest('.card-body').find('.inputOption').val(optionValues);
		});


		var inputOptionValues= $(".inputOption").map(function() {
			return $(this).val();
		}).get();

		var inputOptionString = (inputOptionValues.length > 0) ? inputOptionValues.join() : "";

		var inputOptionArray = inputOptionString.split(',');

		if( inputOptionArray.length > 0 ) {
			$('.options').each ( function() {
				if( $.inArray( $(this).val(), inputOptionArray ) !== -1 ) {
					$(this).attr('checked', true);
				}
			});
		}

	});
</script>
@stop


