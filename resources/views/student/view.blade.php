@extends('student.parent')

@section('main')

                    
@include('alert')

<div align="right">
	<a class="btn btn-primary" href="{{ url('/') }}" class="btn btn-default">Back</a>
</div>
<hr/>

	<div class="panel-group">
   	 	<div class="panel panel-default">
   	 	  	<div class="panel-heading">
				  Student Details
			</div>
   	 	  	
			<div class="panel-body">

				<div class="form-group">
					<div class="col-sm-12">
						<label>Full Name : {{ $studentData['first_name'] . " " . $studentData['last_name'] }}</label>
						<br/>
						<label>Email : {{ $studentData['email'] }}</label>
					</div>       
				</div>
			</div>
   	 	</div>

 	  	<div class="panel panel-default">
 	  	  	<div class="panel-heading">
				MCQ Test for {{ $studentAnswers->first()->questions->categories->name }}
			</div>

 	  	 	<div class="panel-body mcq-lists">
				@php $i = 1; @endphp

				@forelse ($studentAnswers as $answer)
    
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{ $i . "." . $answer->questions->title}}</h4>
						<h5>student Ans: {{ $answer->answer_id }}</h5>
						<h5>Origin Ans: {{ $answer->questions->answer_ids }}</h5>
					</div>
				</div>
				<hr/>

				@php $i++; @endphp

				@empty
					Not found.
				@endforelse
				

				<button class="btn btn-danger" type="button" name="prev">previous</button>
				<button class="btn btn-primary" type="button" name="next">next</button>
				
			</div>
 	  	</div>

 	</div>


@endsection

@section('page-script')
<script type="text/javascript">
	
</script>
@stop


