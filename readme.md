## Online Examination System - Laravel 5.8 | MySQL


### TECHNOLOGY

* **Laravel**: Laravel 5.8 - PHP 7.2 or higher.
* **Twitter Bootstrap**: V4.1.1 use for css styling and js.
* **MySQL**: Database.
* **jQuery/**: Front end framework.

### Installation and Configuration

**1. Enter git clone and the repository URL at your command line::** 
~~~
git clone https://bhupesh19921@bitbucket.org/bhupesh-kushwaha/online-examination-system.git
~~~

**2. Goto online-examination-system directory and composer update:** 
~~~
composer update
~~~

**3. Copy `env.example` to `.env` and generate app key:** 
~~~
cp .env.example .env

php artisan key:generate
~~~

**3.1. You need to set your `APP_NAME` and `APP_URL` from `.env` file** 

**4. Create a database `online_examination_system_db` or if you want to change database name just go in .env file and change value for `DB_DATABASE` key:**

**5. Now, Run migration to create a table in your database:** 
~~~
php artisan migration
~~~

**6. Now, Run seed to store survey and question data to start:** 
~~~
php artisan db:seed
~~~

**7. Finally, Start your server:**
~~~
php artisan serve
~~~