<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            [
                'name' => 'Technical'
            ],
            [
                'name' => 'Hardware'
            ],
            [
                'name' => 'Software'
            ],
            [
                'name' => 'Arts'
            ],
            [
                'name' => 'Management'
            ]
        ]);
    }
}
