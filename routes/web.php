<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'studentController@index');

Route::get('/mcq/{id}', 'studentController@showMCQ');
Route::post('/mcq/save', 'studentController@saveMCQ')->name('mcqSave');
Route::get('/mcq/student/result', 'studentController@viewMCQ')->name('mcqView');

Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);

Route::get('/admin/home', 'HomeController@index')->name('home');

Route::post('/admin/home', 'HomeController@saveQuestion')->name('saveQuestion');